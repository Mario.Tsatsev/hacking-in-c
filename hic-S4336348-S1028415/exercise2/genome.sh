#!/bin/sh
#$1 is the name of the file.
#$2 is the string the program searches for.

cat $1 | tr -d "\n" | grep -o $2 | wc -l
