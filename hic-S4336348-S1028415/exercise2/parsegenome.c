#include<stdio.h>

int check_format (char *filename) {
    FILE *fp;
    fp = fopen (filename, "r");
    signed int rightFormat = 0;
    unsigned int countLine = 0;
    unsigned int countChar = 0;
    for (char c = getc(fp); c != EOF; c = getc(fp)) {
        if (c != '\n') {
            countChar++;
        }
        else {
            if (countChar != 100) {
                rightFormat = -1;
            }
            countChar = 0;
            countLine++;
        }
    }
    if (countLine != 500) {
        rightFormat = -1;
    }
    int fclose (FILE *fp);
    return rightFormat;
}

void charOccurs (int format, char *filename) {
    FILE *fp;
    fp = fopen (filename, "r");
    signed int noA = 0;
    signed int noC = 0;
    signed int noG = 0;
    signed int noT = 0;
    if (format == 0) {
        for (char c = getc(fp); c != EOF; c = getc(fp)) {
            if (c == 'A') {
                noA++;
            }
            else if (c == 'C') {
                noC++;
            }
            else if (c == 'G') {
                noG++;
            }
            else if (c == 'T') {
                noT++;
            }
        }
        printf ("Occurances of A: %d\n", noA);
        printf ("Occurances of C: %d\n", noC);
        printf ("Occurances of G: %d\n", noG);
        printf ("Occurances of T: %d\n", noT);
    }
    else {
        printf ("File is not in right format.");
    }
    int fclose (FILE *fp);
}

int main (int argc, char *argv[]) {
    signed int format = check_format (argv[1]);
    charOccurs (format, argv[1]);
    return 0;
}
