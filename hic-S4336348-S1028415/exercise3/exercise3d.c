#include<stdio.h>
#include<stdint.h>
#include<string.h>
#include<stdlib.h>

void rndm () {
	FILE *fp;	
	fp = fopen ("/dev/urandom", "r");
	unsigned int i = 0;
	unsigned int j = 0;
	char str1[20];
	char str2[20];
	unsigned int randNum = 0;
	while (randNum != 42) {
		i = getc (fp);
		j = getc (fp);
		sprintf (str1, "%d", i);
		sprintf (str2, "%d", j);
		strcat (str1, str2);
		randNum = atoi (str1);
		if (randNum < 16) {
			printf ("000%x\n", randNum);
		}  
		else if (randNum < 256) {
			printf ("00%x\n", randNum);
		}
		else if (randNum < 4096) {
			printf ("0%x\n", randNum);
		}
		else if (randNum < 65536){
			printf ("%x\n", randNum);
		}
	}
	int fclose (FILE *fp);
}

int main () {
	rndm();
	return 0;
}
