#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

int main() {
	int32_t a = 1;
	int16_t b = 2;
	unsigned char c = 3;
	int8_t d = 4;
	unsigned long long e = 5;
	short f[3] = {6, 6, 6};
	long g = 7;
	unsigned long int h = 8;
	uint8_t i = 9;

	fprintf(stderr, "address\t\tvariable\tvalue\tsizeof\tnext_addr\n");
	printf("%p\t\t%s\t%d\t%ld\t%p\n", &a, "a", a, sizeof(a), (&a) +1);
	printf("%p\t\t%s\t%d\t%ld\t%p\n", &b, "b", b, sizeof(b), (&b) +1);
	printf("%p\t\t%s\t%d\t%ld\t%p\n", &c, "c", c, sizeof(c), (&c) +1);
	printf("%p\t\t%s\t%d\t%ld\t%p\n", &d, "d", d, sizeof(d), (&d) +1);
	printf("%p\t\t%s\t%lld\t%ld\t%p\n", &e, "e", e, sizeof(e), (&e) +1);
	printf("%p\t\t%s\t%hn\t%ld\t%p\n", &f, "f", f, sizeof(f), (&f) +1);
	printf("%p\t\t%s\t%ld\t%ld\t%p\n", &g, "g", g, sizeof(g), (&g) +1);
	printf("%p\t\t%s\t%ld\t%ld\t%p\n", &h, "h", h, sizeof(h), (&h) +1);
	printf("%p\t\t%s\t%d\t%ld\t%p\n", &i, "i", i, sizeof(i), (&i) +1);
	return 0;
}






