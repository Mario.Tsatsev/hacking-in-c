#include <stdio.h>
#include <stdbool.h>

int main() {
	bool myBool;
	printf("Size in bytes: %ld\n", sizeof(myBool));
	myBool = true;
	printf("Hexadecimal representation of true: %x\n", myBool);
	myBool = false;
	printf("Hexadecimal representation of false: %x\n", myBool);
	myBool = 0xF0;
	printf("0xF0 value interpreted as: %x\n", myBool);
	printf("%s", "It appears to be the case that all other values besides 0x0 are interpreted as true\n");
	return 0;
}
