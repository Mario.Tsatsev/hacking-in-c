#include<stdio.h>
#include<stdlib.h>

int main(int argc, char const *argv[]) {

  int one = 1;
  if((- one) == ~one){
    printf("This computer doesn't use two-complement representation\n");
  }else{
    printf("This computer uses two-complement representation\n");
  }
  //the approach was to see if flipping the bits ~ would result in the same answer as negating it which it did not.
  return 0;
}
