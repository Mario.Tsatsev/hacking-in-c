#include<stdio.h>
#include<stdlib.h>

int main(void) {
  short i = 0x1234;
  char  x = -127;
  long sn1 = 1028415;
  long sn2 = 4336348;
  int y[2] = {0x11223344,0x44332211};
  printf("address\t content(hex)\t content(dec)\n");

  unsigned char* b = (char*) &i;
  for( int n = 0; n < sizeof(i); n++, b ++){
    printf("%p\t%x\t%d\n",b, *b,*b );
  }

  unsigned char* c = (char*) &x;
  for( int n = 0; n < sizeof(x); n++, c ++){
    printf("%p\t%x\t%d\n",c, *c,*c );
  }

  unsigned char* d = (char*) &sn1;
  for( int n = 0; n < sizeof(sn1); n++, d ++){
    printf("%p\t%x\t%d\n",d, *d,*d );
  }

  unsigned char* e = (char*) &sn2;
  for( int n = 0; n < sizeof(sn2); n++, e ++){
    printf("%p\t%x\t%d\n",e, *e,*e );
  }

  unsigned char* r = (char*) &y;
  for( int n = 0; n < sizeof(y); n++, r ++){
    printf("%p\t%x\t%d\n",r, *r,*r );
  }
  return 0;
}
