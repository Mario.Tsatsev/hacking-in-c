#include<stdio.h>

void addVector(int *r, const int *a, const int* b, unsigned int len){
  unsigned int *i;

  int *aa = &(a[0]);
  int *bb = &(b[0]);
  for(i = &(r[0]); i != &(r[len]); i++){
    *i = *aa + *bb;
    aa ++;
    bb ++;
  }
}
int memcmp (const void* a, const void* b, size_t len) {
	size_t* r;
	int* q = &(a[0]);
	int* s = &(b[0]);
	for (signed int* p = &(r[0]); p != &(r[len]); p++, q++, s++) {
		*p = *q - *s;
	}
	if (*r < 0) {
		return -1;
	}
	else if (*r > 0) {
		return 1;
	}
	else {
		return 0;
	}
}

int memcmp_backwards (const void* a, const void* b, size_t len) {
	int a_size = (int)sizeof(a);
	int b_size = (int)sizeof(b);
	size_t* r;
	int* q = &(a[a_size-1]);
	int* s = &(b[b_size-1]);
	for (int* p = &(r[0]); p < &(r[len]); p++, q--, s--) {
		*p = *q - *s;
	}
	if (*r < 0) {
		return -1;
	}
	else if (*r > 0) {
		return 1;
	}
	else {
		return 0;
	}
}

int main(int argc, char const *argv[]) {
  int r[3] = {0,0,0};
  int b[3] = {1,2,3};
  int a[3] = {1,2,3};

  addVector(r,a,b,3);
  for (int i = 0; i < 3; i++) {
    printf("%d\n",r[i]);
  }
  int k = memcmp_backwards(a,a,sizeof(r));
  printf("%d\n",k);
  return 0;
}
