#include<stdio.h>

extern void magic_function();

void maxFunctionSize() {
	unsigned char byteArray[4000000];
	for (unsigned int i = 0; i < 4000000; i++) {
		byteArray[i] = 'a';
	}
}

void findFunctionSize() {
	unsigned char byteArray[4000000];
	unsigned int i = 3999999;
	unsigned int stop = 0;
	unsigned int size;
	while (stop != 1) {
		if (byteArray[i] == 'a') {
			size = 4000000 - i;
			stop = 1;
		}
		i--;
	}
	printf("%d\n", size);
}


int main (void)
{
	maxFunctionSize();
	magic_function();
	findFunctionSize();
}
