#include <stdio.h>
#include <stdlib.h>

int main() {
	void *ptr = calloc(1, 1);
	int increment = 32784;
	int i = 1;
	while(ptr != NULL) {
		ptr = calloc(1, i*increment);
		i++;
	}
	free(ptr);
	printf("One malloc can allocate at most %ld bytes\n", i*increment);
}
