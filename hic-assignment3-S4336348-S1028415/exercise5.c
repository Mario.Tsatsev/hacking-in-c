#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(void) {
	char *s1 = malloc(9);
	if (s1 == NULL) return 1;
	char *s2 = malloc(9);
	if (s2 == NULL) return 1;

	strcpy(s1, "s4336348");
	strcpy(s2, "s1028415");

	// do your attack
	// Both strings are likely to be right after another in memory. So s2 will likely be located at byte 10 up to 18 if byte 1 up to 9 is s1. If we want printf to print both numbers we need to remove the \0 from s1 and printf will continue reading until the /0 from s2.

	for(int i = 0; i < 32; i++) {
		char *p = s1 + i;
		if (*p == 0 || *p == '!') {
			*p = ' ';
		}
	}
	//s1[strlen(s1)] = ' ';
	printf("student 1: %s\n", s1);
	printf("student 2: %s\n", s2);
	return 0;
}

//Well, I couldn't get it to work. When I try to replace the last byte in the array it just adds the byte and still pads the char[] with a '\0' at the end.

// answer to 5b) We need strcopy because just using char *s1 = "someString" would lead to omitting the null-terminator at the end.
// answer to 5c) We need to allocate 9 bytes to host 8-character strings because the 9th byte is used to store the null-terminator \0.
